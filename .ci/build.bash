#!/bin/bash
set -e
set -o xtrace

whoami
env | sort

# Build 
mkdir -p build && cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/.local -DNRP_SKIP_INSTALL_THIRDPARTY_LIB=1 ..
make -j`nproc`
make package
