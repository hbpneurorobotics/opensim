#ifndef OPENSIM_MYO_MUSCLE_H_
#define OPENSIM_MYO_MUSCLE_H_
/* -------------------------------------------------------------------------- *
 *                      OpenSim:  MyoroboticsMuscle.h                        *
 * -------------------------------------------------------------------------- *
 * The OpenSim API is a toolkit for musculoskeletal modeling and simulation.  *
 * See http://opensim.stanford.edu and the NOTICE file for more information.  *
 * OpenSim is developed at Stanford University and supported by the US        *
 * National Institutes of Health (U54 GM072970, R24 HD065690) and by DARPA    *
 * through the Warrior Web program.                                           *
 *                                                                            *
 * Copyright (c) 2005-2013 Stanford University and the Authors                *
 * Author(s): Ajay Seth                                                       *
 *                                                                            *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may    *
 * not use this file except in compliance with the License. You may obtain a  *
 * copy of the License at http://www.apache.org/licenses/LICENSE-2.0.         *
 *                                                                            *
 * Unless required by applicable law or agreed to in writing, software        *
 * distributed under the License is distributed on an "AS IS" BASIS,          *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 * See the License for the specific language governing permissions and        *
 * limitations under the License.                                             *
 * -------------------------------------------------------------------------- */
#include <OpenSim/Actuators/osimActuatorsDLL.h>
#include <OpenSim/Simulation/Model/PathActuator.h>

#define M_PI           3.14159265358979323846  /* pi */

//=============================================================================
//=============================================================================
namespace OpenSim { 

/**
 */
class OSIMACTUATORS_API MyoroboticsMuscle : public PathActuator {
OpenSim_DECLARE_CONCRETE_OBJECT(MyoroboticsMuscle, PathActuator);
public:
//=============================================================================
// PROPERTIES
//=============================================================================
	OpenSim_DECLARE_PROPERTY(initial_musclestretch, double,
		"The initial muscle stretch (m) of the spring element.");
	OpenSim_DECLARE_PROPERTY(initial_angvel, double,
		"The initial stretch (m) of the spring element.");
	OpenSim_DECLARE_PROPERTY(initial_current, double,
		"The initial stretch (m) of the spring element.");
//=============================================================================
// PUBLIC METHODS
//=============================================================================
	MyoroboticsMuscle();
	
	/** Convenience constructor with MyoroboticsMuscle parameters
     * @param name          	the name of a %MyoroboticsMuscle instance
	 * @param musclestretch0 	The change in length of the tendon from muscle
	 * 							exit point to robot attachment point in [m]
	 * @param angvel			the the motor angvel in [1/s] 
	 * @param currnet	     	the motor current in [V] */
	MyoroboticsMuscle(const std::string& name, 
				double musclestretch0=0.0, 
				double angvel0=0.0, 
				double current0=0.0);

    // default destructor, copy constructor, copy assignment

	//--------------------------------------------------------------------------
	//  <B> Get and set MyoroboticsMuscle properties </B>
	//--------------------------------------------------------------------------

	/** Initial spring stretch in m. */
	double getInitialMuscleStretch() {return get_initial_musclestretch(); }
	void setInitialMuscleStretch(double musclestretch0);
	double getInitialAngvel() {return get_initial_angvel(); }
	void setInitialAngvel(double angvel0);
	double getInitialCurrent() {return get_initial_current(); }
	void setInitialCurrent(double current0);

	//--------------------------------------------------------------------------
	//  <B> State dependent values </B>
	//--------------------------------------------------------------------------
	
	double getMuscleStretch(const SimTK::State& s) const;
	double getAngvel(const SimTK::State& s) const;
	double getCurrent(const SimTK::State& s) const;

	double getTension(const SimTK::State& s) const;

	double getSpringDisplacement(const SimTK::State& s) const;
	double getGearPosition(const SimTK::State& s) const;

protected:
	struct MyoroboticsMuscleInfo{	    //DIMENSION         Units      
        double springDisplacement;          //length            [m]			[1]
        double tendonDisplacemant;          //length            [m]			[2]

		double alpha_1; 					//angle 			[radian]
		double alpha_2; 					//angle 			[radian]
		double length_1;					//length			[m]
		double length_2; 					//length 			[m]

		double initialInternalTendonLength;	//length			[m]
        double internalTendonLength;        //length            [m]
		
		double tendonTension;				//force				[N]			[3]

				// previous simulation time
		double prevSimTime;					//required for gearposition calculation
		double gearPosition;				//rotation			[1]			[6]

		bool firstUpdate;
		
		SimTK::Vector userDefinedLengthExtras;//NA        NA

		MyoroboticsMuscleInfo(): 
            userDefinedLengthExtras(0, SimTK::NaN){}
		friend std::ostream& operator<<(std::ostream& o, 
            const MyoroboticsMuscleInfo& seeI) {
			o << "MyoroboticsMuscle::MyoroboticsMuscleInfo should not be serialized!" 
              << std::endl;
			return o;
		}
	};
	/** Developer Access to intermediate values calculate by the muscle model */
	const MyoroboticsMuscleInfo& getMyoroboticsMuscleInfo(const SimTK::State& s) const;
	MyoroboticsMuscleInfo& updMyoroboticsMuscleInfo(const SimTK::State& s) const;
	void calcMyoroboticsMuscleInfo(const SimTK::State& s, MyoroboticsMuscleInfo& myoI) const;

	struct Motor {
		Motor(){
		voltage = 24;			//[V] max voltage
        torqueConst = 14.2e-03; // [Nm/A]
        resistance = 0.797 + 0.12; // [Ohm] +0.12 for PWM simulation
        inductance = 0.118e-03; // [H]
        BEMFConst = 14.2e-03; // [V/s]
        inertiaMoment = 4.09e-06; // [kgm^2]
		continuousTorque = 47.6e-03; // [Nm]
		stallTorque	= 857e-03; //[Nm]
		continuousCurrent = 3.45; // [A]
		stallCurrent	= 60.2; //[A]
		speed_torque_gradient = 626.667; // [rps/Nm] = 37.6[rpm/mNm]
		}
		double voltage;		//[V] max voltage
        double torqueConst; // [Nm/A]
        double resistance; // [Ohm] +0.12 for PWM simulation
        double inductance; // [H]
        double BEMFConst; // [V/s]
        double inertiaMoment; // [kgm^2]
		double continuousTorque; // [Nm]
		double stallTorque; //[Nm]
		double continuousCurrent; // [A]
		double stallCurrent; //[A]
		double speed_torque_gradient; // [rps/Nm] = 37.6[rpm/mNm]
	} motor;
	struct Gear {
		Gear(){
		inertiaMoment = 0.4e-07; // [kgm^2]
        ratio = 53; // [1]
        efficiency = 0.59; // [1]
		}
        double inertiaMoment; // [kgm^2]
        double ratio; // [1]
        double efficiency; // [1]
	} gear;
	struct Spindle {
		Spindle(){
			radius = 4.5e-03; // [m]
		}
        double radius; // [m]
	} spindle;
	struct SEE {
		SEE(){
			springStiffness = 30680.0; // N/m
			tendonStiffness = 1e6; // N/m ( random high nummber )
			maxDisplacement = 0.02; //m
			c1 = 0.012; //m
			c2 = 0.008; //m
			c3 = 0.018; //m
			c4 = 0.039; //m
			length_c1 = 0.04; //m
			length_c2 = 0.013;//m
		}

		double springStiffness; // N/m
		double tendonStiffness; // N/m
		double maxDisplacement; //m
		// c1-c4 are valuesdescribing the see internal geometry 
		double c1, c2, c3, c4; //m
		// length_c* are constand tendonlengths inside the motor
		double length_c1, length_c2; //m
	} see;
	//--------------------------------------------------------------------------
	// COMPUTATIONS
	//--------------------------------------------------------------------------

	double computeActuation( const SimTK::State& s) const OVERRIDE_11;

    /** Override PathActuator method to calculate a color for use when
    the %MyoroboticsMuscle's path is displayed in the visualizer. 
    @param[in] state    
        A SimTK::State already realized through Stage::Velocity. Do not 
        attempt to realize it any further.
    @returns 
        The desired color for the path as an RGB vector with each
        component ranging from 0 to 1, or NaN to indicate that the color
        should not be changed. **/
    SimTK::Vec3 computePathColor(const SimTK::State& state) const OVERRIDE_11;

    /** Implement ModelComponent interface. */
	void addToSystem(SimTK::MultibodySystem& system) const OVERRIDE_11;
	void initStateFromProperties(SimTK::State& state) const OVERRIDE_11;
	void setPropertiesFromState(const SimTK::State& state) OVERRIDE_11;
	Array<std::string> getStateVariableNames() const OVERRIDE_11;
	SimTK::SystemYIndex getStateVariableSystemIndex(
							const std::string& stateVariableName) const OVERRIDE_11;

	SimTK::Vector computeStateVariableDerivatives(
							const SimTK::State& s) const OVERRIDE_11;


private:
	void setNull();
	void constructProperties();

	double EfficiencyApproximation(const SimTK::State& s) const;

	void initMyoroboticsMuscle(const SimTK::State& s, MyoroboticsMuscleInfo& myoI) const;
	void updateSEE(const SimTK::State& s, MyoroboticsMuscleInfo& myoI) const;
//=============================================================================
};	// END of class MyoroboticsMuscle

}; //namespace
//=============================================================================
//=============================================================================

#endif // OPENSIM_MyoroboticsMuscle_H_



