/* -------------------------------------------------------------------------- *
 *                      OpenSim:  MyoroboticsMuscle.cpp                      *
 * -------------------------------------------------------------------------- *
 * The OpenSim API is a toolkit for musculoskeletal modeling and simulation.  *
 * See http://opensim.stanford.edu and the NOTICE file for more information.  *
 * OpenSim is developed at Stanford University and supported by the US        *
 * National Institutes of Health (U54 GM072970, R24 HD065690) and by DARPA    *
 * through the Warrior Web program.                                           *
 *                                                                    *
 * Copyright (c) 2005-2012 Stanford University and the Authors                *
 * Author(s): Ajay Seth                                                     *
 *                                                                            *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may    *
 * not use this file except in compliance with the License. You may obtain a  *
 * copy of the License at http://www.apache.org/licenses/LICENSE-2.0.         *
 *                                                                            *
 * Unless required by applicable law or agreed to in writing, software        *
 * distributed under the License is distributed on an "AS IS" BASIS,          *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 * See the License for the specific language governing permissions and        *
 * limitations under the License.                                             *
 * -------------------------------------------------------------------------- */

//=============================================================================
// INCLUDES
//=============================================================================
#include "MyoroboticsMuscle.h"


//=============================================================================
// STATICS
//=============================================================================
using namespace std;
using namespace OpenSim;
using SimTK::Vec3;

static const Vec3 DefaultMyoroboticsMuscleColor(.9,.9,.9); // mostly white 

//=============================================================================
// CONSTRUCTOR(S) AND DESTRUCTOR
//=============================================================================
//_____________________________________________________________________________
// Default constructor.
MyoroboticsMuscle::MyoroboticsMuscle()
{
	constructProperties();
}

MyoroboticsMuscle::MyoroboticsMuscle(const string& name, double musclestretch0, double angvel0, double current0)
{
	constructProperties();
	setName(name);
	set_initial_musclestretch(musclestretch0);
	set_initial_angvel(angvel0);
	set_initial_current(current0);
	motor = Motor();
	gear = Gear();
    spindle = Spindle();
	see = SEE();
}

//_____________________________________________________________________________
/*
 * Construct and initialize the properties for the MyoroboticsMuscle.
 */
void MyoroboticsMuscle::constructProperties()
{
	setAuthors("");
	
	setMinControl(0.0);
	setMaxControl(1.0);
	constructProperty_initial_musclestretch(0.0);
	constructProperty_initial_angvel(0.0);
	constructProperty_initial_current(0.0);

	setOptimalForce(1.0);
}

//_____________________________________________________________________________

/*
 * Set the initial musclestretch.
 */
void MyoroboticsMuscle::setInitialMuscleStretch(double musclestretch0)
{
	set_initial_musclestretch(musclestretch0);
}
void MyoroboticsMuscle::setInitialAngvel(double angvel0)
{
	set_initial_angvel(angvel0);
}
void MyoroboticsMuscle::setInitialCurrent(double current0)
{
	set_initial_current(current0);
}

//_____________________________________________________________________________

//_____________________________________________________________________________
/**
 * allocate and initialize the SimTK state for this MyoroboticsMuscle.
 */
 void MyoroboticsMuscle::addToSystem(SimTK::MultibodySystem& system) const
{
	Super::addToSystem(system);
	// The spring force is dependent of musclestretch so only invalidate dynamics
	// if the musclestretch state changes
	addStateVariable("musclestretch");
	addStateVariable("current");
	addStateVariable("angvel");
	
	addCacheVariable<MyoroboticsMuscle::MyoroboticsMuscleInfo>
       ("MyoroboticsMuscleInfo", MyoroboticsMuscleInfo(), SimTK::Stage::Velocity);
}

 void MyoroboticsMuscle::initStateFromProperties(SimTK::State& state) const
 {
	setStateVariable(state, "musclestretch", get_initial_musclestretch());
	setStateVariable(state, "current", get_initial_current());
	setStateVariable(state, "angvel", get_initial_angvel());
	
 }

 void MyoroboticsMuscle::setPropertiesFromState(const SimTK::State& state)
 {
	set_initial_musclestretch(getMuscleStretch(state));
	set_initial_current(getCurrent(state));
	set_initial_angvel(getAngvel(state));
	
 }
 
 Array<std::string> MyoroboticsMuscle::getStateVariableNames() const
 {
	 Array<std::string> stateVariableNames = ModelComponent::getStateVariableNames();
	 // Make state variable names unique to this muscle
	 for(int i=0; i<stateVariableNames.getSize(); ++i){
		 stateVariableNames[i] = getName()+"."+stateVariableNames[i];
	 }
	 return stateVariableNames;
 }

 SimTK::SystemYIndex MyoroboticsMuscle::
	 getStateVariableSystemIndex(const string& stateVariableName) const
 {
	 unsigned start = (unsigned)stateVariableName.find(".");
	 unsigned end = (unsigned)stateVariableName.length();

	 if(start == end)
		 return ModelComponent::getStateVariableSystemIndex(stateVariableName);
	 else{
         ++start;
		 string localName = stateVariableName.substr(start, end-start);
		 return ModelComponent::getStateVariableSystemIndex(localName);
	 }
 }


//=============================================================================
// GET AND SET
//=============================================================================
//-----------------------------------------------------------------------------
// STRETCH and TENSION
//-----------------------------------------------------------------------------

double MyoroboticsMuscle::getMuscleStretch(const SimTK::State& s) const
{
	return getStateVariable(s, "musclestretch");
}
double MyoroboticsMuscle::getAngvel(const SimTK::State& s) const
{
	return getStateVariable(s, "angvel");
}
double MyoroboticsMuscle::getCurrent(const SimTK::State& s) const
{
	return getStateVariable(s, "current");
}
double MyoroboticsMuscle::getTension(const SimTK::State& s) const
{
	// evaluate tension in the spring
	// note tension is positive and produces shortening
	// damping opposes lengthening, which is positive lengthening speed
	// there for stretch and lengthening speed increase tension
	return getForce(s); 
}
double MyoroboticsMuscle::getSpringDisplacement(const SimTK::State& s) const{
	const MyoroboticsMuscleInfo& myoI = getCacheVariable<MyoroboticsMuscleInfo>(s, "MyoroboticsMuscleInfo");
	return myoI.springDisplacement;
}
double MyoroboticsMuscle::getGearPosition(const SimTK::State& s) const{
	const MyoroboticsMuscleInfo& myoI = getCacheVariable<MyoroboticsMuscleInfo>(s, "MyoroboticsMuscleInfo");
	return myoI.gearPosition;
}

//-----------------------------------------------------------------------------
// COMPUTATIONS
//-----------------------------------------------------------------------------
double MyoroboticsMuscle::computeActuation(const SimTK::State& s) const
{
	const MyoroboticsMuscleInfo& myoI = getMyoroboticsMuscleInfo(s);
	double tension = myoI.tendonTension;
	
	//this is for testing output:
	//double tension = getAngvel(s);//getCurrent(s) ; //getStretch(s) *
	//std::cout << tension << std::endl;

	setForce(s, tension);
	return tension;
}

SimTK::Vector MyoroboticsMuscle::
	computeStateVariableDerivatives(const SimTK::State& s) const //TODO
{
	// clamp or cap the control input to [0, 1]
	double control = SimTK::clamp(0.0, getControl(s), 1.0) * motor.voltage; 

	SimTK::Vector derivs(3, 0.0);
		
	//muscle tendon stretch
	derivs[0] = getLengtheningSpeed(s);

	double gearAppEfficiency = EfficiencyApproximation(s);
	double totalIM = motor.inertiaMoment + gear.inertiaMoment; // total moment of inertia
	
	derivs[1] = 1 / motor.inductance * (-motor.resistance * getCurrent(s)
											-motor.BEMFConst * gear.ratio * getAngvel(s)
											+control );
	derivs[2] = motor.torqueConst * getCurrent(s) / (gear.ratio * totalIM) - spindle.radius * getTension(s)/
               										(gear.ratio * gear.ratio * totalIM * gearAppEfficiency);
	return derivs;
} 

SimTK::Vec3 MyoroboticsMuscle::computePathColor(const SimTK::State& state) const
{
	double shade = SimTK::clamp(0.1, getControl(state), 1.0);
    const SimTK::Vec3 color(shade, 0.9, 0.1); // green to yellow
    return color;
}
//==========================================================================
//			MyoroboticsMuscleInfo
//==========================================================================
/* Access to muscle calculation data structures */
const MyoroboticsMuscle::MyoroboticsMuscleInfo& MyoroboticsMuscle::getMyoroboticsMuscleInfo(const SimTK::State& s) const
{
	if(!isCacheVariableValid(s,"MyoroboticsMuscleInfo")){
		MyoroboticsMuscleInfo &myoI = updMyoroboticsMuscleInfo(s);
		calcMyoroboticsMuscleInfo(s, myoI);
		markCacheVariableValid(s,"MyoroboticsMuscleInfo");
		// don't bother fishing it out of the cache since 
		// we just calculated it and still have a handle on it
		return myoI;
	}
	return getCacheVariable<MyoroboticsMuscleInfo>(s, "MyoroboticsMuscleInfo");
}

MyoroboticsMuscle::MyoroboticsMuscleInfo& MyoroboticsMuscle::updMyoroboticsMuscleInfo(const SimTK::State& s) const
{
	return updCacheVariable<MyoroboticsMuscleInfo>(s, "MyoroboticsMuscleInfo");
}

void MyoroboticsMuscle::calcMyoroboticsMuscleInfo(const SimTK::State& s, MyoroboticsMuscleInfo& myoI) const
{
	if(myoI.firstUpdate){
		initMyoroboticsMuscle(s, myoI);
	}
	// gear position update
	double period = s.getTime() - myoI.prevSimTime;
	myoI.prevSimTime = s.getTime();
	myoI.gearPosition += getAngvel(s) * period;

	//std::cout << myoI.tendonTension << std::endl;
	updateSEE(s, myoI);
}

//==========================================================================
//	                   Helper
//==========================================================================
double MyoroboticsMuscle::EfficiencyApproximation(const SimTK::State& s) const{
    double param1 = 0.1; // defines steepness of the approximation
	double param2 = 0; // defines zero crossing of the approximation
	return gear.efficiency + (1 / gear.efficiency - gear.efficiency) *
								 (0.5 * (tanh(-param1 * getAngvel(s) * getCurrent(s) - param2) + 1));
}

void MyoroboticsMuscle::initMyoroboticsMuscle(const SimTK::State& s, MyoroboticsMuscleInfo& myoI) const{
	myoI.firstUpdate = false;
	myoI.prevSimTime = s.getTime();
	myoI.springDisplacement = 0.0;
	
	//calculate initial geometry state
	myoI.alpha_1 = std::atan( see.c1 / (see.c4-myoI.springDisplacement) );
	myoI.alpha_2 = std::atan( see.c2 / (see.c3+see.c4-myoI.springDisplacement) );  
	myoI.length_1 = sqrt( see.c1*see.c1 + (see.c4-myoI.springDisplacement)*(see.c4-myoI.springDisplacement) );
	myoI.length_2 = sqrt( see.c2*see.c2 + (see.c3+see.c4-myoI.springDisplacement)*(see.c3+see.c4-myoI.springDisplacement) );
	
	myoI.initialInternalTendonLength = see.length_c1 + see. length_c2 + myoI.length_1 + myoI.length_2;

}
void MyoroboticsMuscle::updateSEE(const SimTK::State& s, MyoroboticsMuscleInfo& myoI) const{

	double motorStretch = myoI.gearPosition * 2*M_PI*spindle.radius; 
	//update the internal tandon length to adjust to the new muscle length
	myoI.internalTendonLength = myoI.initialInternalTendonLength - getMuscleStretch(s) - motorStretch;

	//update the spring displacement
	myoI.springDisplacement = see.c4 - ( (myoI.internalTendonLength - see.length_c1 - see.length_c2 
		- see.c3/std::cos(myoI.alpha_2)) / (1/std::cos(myoI.alpha_1) + 1/std::cos(myoI.alpha_2)) );
	
	//check for physical displacement limits.
	myoI.springDisplacement = SimTK::clamp(0.0, myoI.springDisplacement, see.maxDisplacement); 
	//update internal geometry
	myoI.alpha_1 = std::atan( see.c1 / (see.c4-myoI.springDisplacement) );
	myoI.alpha_2 = std::atan( see.c2 / (see.c3+see.c4-myoI.springDisplacement) );  
	myoI.length_1 = sqrt( see.c1*see.c1 + (see.c4-myoI.springDisplacement)*(see.c4-myoI.springDisplacement) );
	myoI.length_2 = sqrt( see.c2*see.c2 + (see.c3+see.c4-myoI.springDisplacement)*(see.c3+see.c4-myoI.springDisplacement) );
	
	//check wether the spring absorbed the change in muscle length. The tendon absorbs the left over length 
	double reachedInternalLength = see.length_c1 + see. length_c2 + myoI.length_1 + myoI.length_2;
	myoI.tendonDisplacemant = reachedInternalLength - myoI.internalTendonLength;
	
	//calculate the tendon tension composed of the non linera force generated by the spring-pulley-system
	//and the linesr tendon force generated after the spring reaches its max displacement
	myoI.tendonTension = (see.springStiffness*myoI.springDisplacement ) / ( std::cos(myoI.alpha_1) + std::cos(myoI.alpha_2) )+(see.tendonStiffness*myoI.tendonDisplacemant);
}
